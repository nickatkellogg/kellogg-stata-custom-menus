/*
  confint

  VERSION 1.1.0  June2009

*/

program define confint
syntax [if] [in], se(string) pr(string) fc(string) clv(integer) [replace]

tokenize `se'

local vpred `1'
local vsemean `2'
local vseind `3'

tokenize `pr'

local lowpr `1'
local highpr `2'

tokenize `fc'

local lowfc `1' 
local highfc `2'

di " "

if "`replace'"=="replace"  {
   di ".capture drop `vpred'"
   capture drop `vpred'
   di ".capture drop `vsemean'"
   capture drop `vsemean'
   di ".capture drop `vseind'"
   capture drop `vseind'
   di ".capture drop `lowpr'"
   capture drop `lowpr'
   di ".capture drop `highpr'"
   capture drop `highpr'
   di ".capture drop `lowfc'"
   capture drop `lowfc'
   di ".capture drop `highfc'"
   capture drop `highfc'
}

local rdf = e(df_r)

local tailconf = (100 - `clv')/200

local tval = invttail(`rdf',`tailconf')


di "Using `rdf' degrees of freedom"
di "Using t-value of `tval'"

di ".predict `vpred', xb"
predict `vpred', xb
di ".predict `vsemean', stdp"
predict `vsemean', stdp
di ".predict `vseind', stdf"
predict `vseind', stdf


di ".gen `lowfc' = `vpred' - `tval'*`vsemean'"
gen `lowfc' = `vpred' - `tval'*`vsemean'

di ".gen `highfc' = `vpred' + `tval'*`vsemean'"
gen `highfc' = `vpred' + `tval'*`vsemean'

di ".gen `lowpr' = `vpred' - `tval'*`vseind'"
gen `lowpr' = `vpred' - `tval'*`vseind'

di ".gen `highpr' = `vpred' + `tval'*`vseind'"
gen `highpr' = `vpred' + `tval'*`vseind'




end
