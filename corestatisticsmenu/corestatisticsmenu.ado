/*
  Kellogg core statistics ado file

  VERSION 1.5  December 15, 2011
  
  Peter Klibanoff
*/

program define corestatisticsmenu
 version 11

quietly capture {

      win m append separator "stUser"
      win m append submenu "stUser" "Core Statistics Menu"
window menu append submenu "Core Statistics Menu" "Univariate Statistics"

window menu append item "Univariate Statistics" "Standard (ktabstat)" "db ktabstat"
window menu append item "Univariate Statistics" "Custom (tabstat)" "db tabstat"
      win m append separator "Core Statistics Menu"
window menu append submenu "Core Statistics Menu" "Bivariate Statistics"
window menu append item "Bivariate Statistics" "Correlations (correlate)" "db correlate"
window menu append item "Bivariate Statistics" "Bivariate Plots (twoway)" "db twoway"

      win m append separator "Core Statistics Menu"
window menu append item "Core Statistics Menu" "Regression (regress)" "db regress"

window menu append submenu "Core Statistics Menu" "Model Analysis, using most recent regression"

window menu append item "Model Analysis, using most recent regression" "Variance Inflation Factors (vif)" "db vif"
window menu append item "Model Analysis, using most recent regression" "Breusch-Pagan heteroskedasticity test (hettest)" "db hettest"
window menu append item "Model Analysis, using most recent regression" "Plot residuals vs predicted values (rvfplot)" "db rvfplot"
window menu append item "Model Analysis, using most recent regression" "Residuals, outliers and influential observations (inflobs)" "db inflobs"
window menu append item "Model Analysis, using most recent regression" "Jarque-Bera non-normality test (jbtest)" "db jbtest"
window menu append item "Model Analysis, using most recent regression" "Default Durbin-Watson statistic (ddw)" "db ddw"

window menu append submenu "Core Statistics Menu" "Test Hypotheses, using most recent regression"
window menu append item "Test Hypotheses, using most recent regression" "Joint significance (testparm)" "db testparm"
window menu append item "Test Hypotheses, using most recent regression" "Linear combinations of coefficients (klincom)" "db klincom"


      win m append separator "Core Statistics Menu"
window menu append submenu "Core Statistics Menu" "Prediction, using most recent regression"
window menu append item "Prediction, using most recent regression" "using the data sheet (confint)" "db confint"
window menu append item "Prediction, using most recent regression" "by writing out the regression equation (kpredint)" "db kpredint"

win m append separator "Core Statistics Menu"
window menu append submenu "Core Statistics Menu" "Manipulate Variables and Observations"
window menu append item "Manipulate Variables and Observations" "Generate New Variable (generate)" "db generate"
window menu append item "Manipulate Variables and Observations" "Replace/Change Existing Variable (replace)" "db replace"
      win m append separator "Manipulate Variables and Observations"
window menu append item "Manipulate Variables and Observations" "Extended Generate New Variables (egen)" "db egen"
      win m append separator "Manipulate Variables and Observations"
window menu append item "Manipulate Variables and Observations" "Drop Variables (drop / keep)" "db drop_vars"
window menu append item "Manipulate Variables and Observations" "Drop Observations (drop if / keep if)" "db drop_obs"

window menu refresh
}
end
