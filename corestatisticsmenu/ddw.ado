/*
  ddw Default Durbin Watson

  VERSION 1.2.0  July 2009

*/

program define ddw

 
  tempvar vtime
  generate `vtime' = _n
  tsset `vtime'
  dwstat

end
