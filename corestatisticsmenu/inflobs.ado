/*
  inflobs

  VERSION 1.3.0  July2009

*/

program define inflobs
syntax [if] [in], newvar(string) flag(string) [replace]

tokenize `newvar'

local resvar `1'
local stuvar `2'
local lvar `3'
local cookvar `4'

if "`replace'"=="replace"  {
   di ".capture drop `resvar'"
   capture drop `resvar'
   di ".capture drop `stuvar'"
   capture drop `stuvar'
   di ".capture drop `lvar'"
   capture drop `lvar'
   di ".capture drop `cookvar'"
   capture drop `cookvar'
}

di ".predict `resvar', resid"
predict `resvar', resid

di ".predict `stuvar', rstudent"
predict `stuvar', rstudent

di ".predict `lvar', leverage"
predict `lvar', leverage

di ".predict `cookvar', cooksd"
predict `cookvar', cooksd


local nobs = e(N)
local rdf = e(df_r)
local ivars = `nobs' - `rdf' - 1

tokenize `flag'

if "`replace'"=="replace"  {
   di ".capture drop `1'"
   capture drop `1'
   di ".capture drop `2'"
   capture drop `2'
   di ".capture drop `3'"
   capture drop `3'
}


di ".gen `1' = (abs(`stuvar') > invttail(`rdf',.025) & `lvar' < 1) if e(sample)"
gen `1' = (abs(`stuvar') > invttail(`rdf',.025) & `lvar' < 1) if e(sample)  

di ".gen `2' = (abs(`lvar') > 2*(1 - `rdf'/`nobs')) if e(sample)"
gen `2' = (abs(`lvar') > 2*(1 - `rdf'/`nobs')) if e(sample)

di ".gen `3' = ((abs(`cookvar') > invFtail(`ivars',`rdf',.5) & `cookvar' < .) | `lvar' == 1) if e(sample)"
gen `3' = ((abs(`cookvar') > invFtail(`ivars',`rdf',.5) & `cookvar' < .) | `lvar' == 1) if e(sample)


di "Flag values:"
di "Studentized residual > " invttail(`rdf',.025)
di "Leverage > 2 * " 1 - `rdf'/`nobs'
di "Cook's distance > " invFtail(`ivars',`rdf',.5)


end
