/*
  ktabstat 

  VERSION 1.1.0  July 2009

*/

program define ktabstat

 
  preserve /* Ensure data is restored to present form after ktabstat.ado is over */
  quietly destring, replace force /* Temporarily make all variables numeric so tabstat does not choke */
  noisily display "preserve"
  display "destring, replace force"
  display "tabstat _all, s(mean sd semean min median max range skewness kurtosis count)"
  tabstat _all, s(mean sd semean min median max range skewness kurtosis count)

end
