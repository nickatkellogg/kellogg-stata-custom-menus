/*
  computes prediction interval (individual)
  modified from klincom
*/

program define predint, rclass

  version 6.0

	if "`e(prefix)'" != "svy" {
		is_svy
		if `r(is_svy)' {
			_svylc `0'
			return add
			exit
		}
	}

	gettoken token 0 : 0, parse(",= ")

	while `"`token'"'!="" & `"`token'"'!="," {
		if `"`token'"' == "=" {
			di in red _quote "=" _quote /*
			*/ " not allowed in expression"
			exit 198
		}

		local formula `"`formula'`token'"'

		gettoken token 0 : 0, parse(",= ")
	}

	if `"`0'"'!="" {
		local 0 `",`0'"'
		syntax [, Level(cilevel) OR IRr RRr HR EForm SHOW ]
		local nopt : word count `or' `irr' `rrr' `hr' `eform'
		if `nopt' > 1 {
			di in red "only one display option can be specified"
			exit 198
		}
		if `"`show'"' != "" {
			capture assert `"`formula'"' == ""
			local rc = _rc
			capture syntax [, SHOW ]
			local rc = `rc' + _rc
			if `rc' {
				error 198
			}
			di /* blank line */
			est display
			exit
		}
	}
	else {
		local level = $S_level
		local nopt 0
	}

	if "`e(cmd)'"=="logistic" {
		local or "or"
		local nopt 1
	}

	if "`or'"!="" {
		local eform "eform(Odds Ratio)"
		display "Not supported"
		exit
	}
	else if "`irr'"!="" {
		local eform "eform(IRR)"
		display "Not supported"
		exit
	}
	else if "`rrr'"!="" {
		local eform "eform(RRR)"
		display "Not supported"
		exit
	}
	else if "`hr'"!="" {
		local eform "eform(Haz. Ratio)"
		display "Not supported"
		exit
	}
	else if "`eform'"!="" {
		local eform "eform(exp(b))"
		display "Not supported"
		exit
	}

	tempname estname x b V est se

	qui test `formula' = 0 , matvlc(`V')

	matrix colnames `V' = (1)
	matrix rownames `V' = (1)

	if      "`r(chi2)'"!=""  { scalar `x' = r(chi2) }
	else if "`r(F)'"   !=""  { scalar `x' = r(F) }
	else                       scalar `x' = .

	if "`r(df_r)'"!="" { local dof = r(df_r) }
	else                 local dof = .

	if `dof'!=. { local dofopt "dof(`dof')" }

	if "`e(depvar)'"!="" {
		local ndepv : word count `e(depvar)'
		if `ndepv' == 1 { local depn "depn(`e(depvar)')" }
	}

	matrix `b' = get(Rr)
	if `nopt' == 1 & `b'[1,colsof(`b')] != 0 {
		if `"`or'`irr'`rrr'"' == "" {
			local eform eform
		}
		else	local eform
		di in smcl in red /*
*/ "additive constant term not allowed with {cmd:`or'`irr'`rrr'`eform'} option"
		exit 198
	}

        if `b'[1,(colsof(`b')-1)] != 1 {
		display "Error: The constant(_cons) is required for prediction"
		exit
	}

	matrix `b' = e(b)*`b'[1,1..(colsof(`b')-1)]' - `b'[1,colsof(`b')]
	matrix colnames `b' = (1)

	/* Display formula. */
	qui test `formula' = 0, notest

	/* Save values of b and V for S_# macros. */
	if "`eform'"=="" {
		scalar `est' = `b'[1,1]
		scalar `se'  = sqrt(`V'[1,1])
	}
	else {
		scalar `est' = exp(`b'[1,1])
		scalar `se'  = exp(`b'[1,1])*sqrt(`V'[1,1])
	}
	nobreak {
		estimates hold `estname'
		capture noisily break {		/* Post results. */
			est post `b' `V', `dofopt' `depn'
			di
			qui est di, `eform' level(`level')
		}
		local rc = _rc
		estimates unhold `estname'
		if `rc' { exit `rc' }
	}



  /* Individual prediction interval */

          tempname pse
          scalar `pse' = sqrt( e(rss)/`dof' + `se'^2 )
          display "Estimate: " `est'
          display "Individual Prediction Interval (`level'%): " /*
             */ "[ " `est'-`pse'*invttail(`dof',(1-`level'/100)/2)  /*
             */ "   ,   " `est'+`pse'*invttail(`dof',(1-`level'/100)/2) " ]"

  /* Clean up temp variable that sometimes seems to get output to datasheet */
  	
  	capture drop __000000
  	
end
