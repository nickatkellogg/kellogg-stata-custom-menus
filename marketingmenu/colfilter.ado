* February 5, 2012
* This implements a collaborative filtering algorithm from Anand
* Example usage: 
* colfilter v11-v20, train(v1-v10) predvar(validate) id(id) clear
* colfilter v11-v20, train(v1-v10) predvar(validate) id(id) clear graphs
* Example dataset: cfdata.dta

program define colfilter
version 12
syntax varlist(min=1), Train(varlist) Predvar(varname) IDvar(varname) clear [GRaph(name)]

local numvariables: word count `varlist'
tokenize `varlist'

qui putmata x_train=(`train') x_test=(`varlist') if `predvar'==0, replace
qui putmata n_train=(`train') actual=(`varlist') user=(`idvar') if `predvar'==1, replace

mata: exportmatrix=maincalc(x_train, x_test, actual, n_train, user)

clear
getmata (user prod actual average cf ractual raverage rcf)=exportmatrix, force
format %9.1g average cf 
qui replace ractual=. if actual==.
qui replace rcf=. if cf==.
qui replace raverage=. if average==.

qui gen product=""
foreach var of numlist 1/`numvariables' {
	qui replace product="``var''" if prod==`var'
}
drop prod
order user product
display ""
display "Average rating selects the best product" %9.3g `=scalar(pintop1actual_top1naive*100)' " % of the time"
display "CF pick is the best product" %9.3g `=scalar(pintop1actual_top1cf*100)' " % of the time"
display ""
display "Average rating selects one of top 3 best product" %9.3g `=scalar(pintop3actual_top1naive*100)' " % of the time"
display "CF pick is one of top 3 best product" %9.3g `=scalar(pintop3actual_top1cf*100)' " % of the time"
display ""
display "Top 3 average ratings contains the best product" %9.3g `=scalar(pintop1actual_top3naive*100)' " % of the time"
display "Top 3 CF pick contains the best product" %9.3g `=scalar(pintop1actual_top3cf*100)' " % of the time"

local min=`=scalar(minrating)'
local max=`=scalar(maxrating)'

if "`graph'"~="" {
twoway (lfitci actual cf  if cf>=`min'-1 & cf<=`max'+1, yscale(range(`min',`max')) xscale(range(`min',`max'))) (scatter actual cf if cf>=`min'-1 & cf<=`max'+1, by(product, legend(off) title("Predictions from CF")  compact graphregion(color(white))) yscale(range(`min',`max')) xscale(range(`min',`max')) aspectratio(0.8)) (function x, range(actual) n(2)) (lfit actual cf  if cf>=`min'-1 & cf<=`max'+1, yscale(range(`min',`max')) xscale(range(`min',`max'))) , legend(off) xlabel( `min'(1)`max') ylabel( `min'(1)`max') name(`graph',replace)  xtitle("Predicted ratings") ytitle("Actual ratings")

/*
twoway (scatter actual average if cf>=`min'-1 & cf<=`max'+1, by(product, legend(off) title("Prediction is best average-rated product") compact graphregion(color(white))) yscale(range(`min',`max')) xscale(range(`min',`max')) aspectratio(0.8)) (function x, range(actual) n(2)), legend(off) xlabel( `min'(1)`max') ylabel( `min'(1)`max') name(naive_graph, replace)
*/
}
end


version 12:
mata:

real matrix maincalc(real matrix x_train, real matrix x_test, real matrix actual, real matrix n_train, real matrix user)
	{
	x_train= x_train'
	x_test= x_test'
	actual= actual'
	n_train=n_train'
	
	Ntrain=cols(x_train)
	Mtrain=rows(x_train)
	Ntest=cols(actual)
	Mtest=rows(actual)
	
	tmpmean=mm_meancolvar(x_test')[1,.]
	yhat_naive=J(Mtest,Ntest,1):*tmpmean'
	yhat_cf=J(Mtest,Ntest,.)
	
	for (i=1; i<=Ntest;i++) {
	 	yhat_cf[.,i]=cf(n_train[.,i], x_train, x_test)
	 	}
	
	ranktr=-mm_ranks(actual,1,3) :+ rows(actual)+1

	rankna=-mm_ranks(yhat_naive,1,3) :+ rows(yhat_naive)+1
	temp=select((1..Ntest), colmissing(yhat_naive):>0)
	rankna[.,temp]=rankna[.,temp]:*.

	rankcf=-mm_ranks(yhat_cf,1,3) :+ rows(yhat_cf)+1
	temp=select((1..Ntest), colmissing(yhat_cf):>0)
	rankcf[.,temp]=rankcf[.,temp]:*.
	
	st_numscalar("pintop1actual_top1naive",rowsum(colmax((ranktr:==1):==rankna))/ cols(ranktr))
	st_numscalar("pintop1actual_top1cf",rowsum(colmax((ranktr:==1):==rankcf))/ cols(ranktr))

	st_numscalar("pintop3actual_top1naive",rowsum(colmax((ranktr:<=3) :==rankna))/ cols(ranktr))
	st_numscalar("pintop3actual_top1cf",rowsum(colmax((ranktr:<=3) :==rankcf))/ cols(ranktr))
	
	st_numscalar("pintop1actual_top3naive",rowsum(colmax((rankna:<=3) :== ranktr))/ cols(ranktr))
	st_numscalar("pintop1actual_top3cf",rowsum(colmax((rankcf:<=3) :== ranktr))/ cols(ranktr))
	
	st_numscalar("minrating",min((x_train,n_train\x_test,actual)))
	st_numscalar("maxrating",max((x_train,n_train\x_test,actual)))

	exportmatrix=(assignid(actual,user),vec(yhat_naive),vec(yhat_cf),vec(ranktr), vec(rankna), vec(rankcf))
	
	return(exportmatrix)
	}


real matrix cf(real matrix y_train, real matrix x_train, real matrix x_test)
	{
	temp=mm_meancolvar(x_train)
	x_sd=sqrt(temp[2,.])
	x_mean=temp[1,.]
	xx=((x_train \ x_test):-x_mean):/x_sd
	in_sample=(1::rows(xx)):<=rows(x_train)
	outof_sample=(1::rows(xx)):>rows(x_train)
	  
	Ntrain=cols(x_train)
	 	weights=J(Ntrain,1,.)
		for (i=1; i<=Ntrain;i++) {
		weights[i,1]=correlation((y_train,x_train[.,i]))[2,1]
		}
	weights=editmissing(weights, 0)	
	sumweights=sum(abs(weights))
		
	wgt_mean=editmissing(xx,0)*(weights:/sum(abs(weights)))
	scaled_wgt_mean=wgt_mean/sqrt(variance(select(wgt_mean,in_sample)))*sqrt(variance(y_train))
	return(select(scaled_wgt_mean:-mean(select(wgt_mean,in_sample)):+mean(y_train),outof_sample))
	}

real matrix melt(real matrix xm)
	{
	udim=cols(xm)
	pdim=rows(xm)
	uindex=J(pdim,udim,.)
	pindex=J(pdim,udim,.)
	
	for (u=1; u<=udim; u++) {
	    for (p=1; p<=pdim; p++) 
	    {
	       uindex[p,u]=u
	       pindex[p,u]=p
	       }
		}
	xmnew=(vec(uindex),vec(pindex), vec(xm))
	return(xmnew)
	}

real matrix assignid(real matrix xm, real vector user)
	{
	udim=cols(xm)
	pdim=rows(xm)
	uindex=J(pdim,udim,.)
	pindex=J(pdim,udim,.)
	
	for (u=1; u<=udim; u++) {
	    for (p=1; p<=pdim; p++) 
	    {
	       uindex[p,u]=user[u]
	       pindex[p,u]=p
	       }
		}
	xmnew=(vec(uindex),vec(pindex), vec(xm))
	return(xmnew)
	}

end
