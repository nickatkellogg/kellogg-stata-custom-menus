program define conjoint
version 11
syntax [varlist], Attributes(varlist) Ratings(varlist) [SAVEPRed(string asis)] [SAVEPW(string asis)] [asis] [holdout(varname)]
set more off 
tempfile profile_mergefile partworths attributes_in_order

if "`holdout'"~="" {
	qui count if `holdout'==1
	local num_holdout_obs=r(N)
	}
else local num_holdout_obs=0

keep `attributes' `ratings' `holdout'
qui ds `attributes', has(type string)
local stringattributevariables="`r(varlist)'"

foreach var of local stringattributevariables {
	qui gen `var'temp1=regexr(trim(itrim(`var')),"[^0-9a-zA-Z_ ]","")
	qui replace `var'=`var'temp1
	qui gen `var'temp2=real(`var'temp1)
	qui gen `var'temp3=missing(`var'temp2)
	qui egen `var'temp4=sum(`var'temp3)
	if  `var'temp4[1]==0 {
		drop `var'
		rename `var'temp2 `var'
	}
	drop `var'temp*
}
order `attributes'

preserve
	clear
	local numattributes: word count `attributes'
	qui gen attribute=""
	qui set obs `numattributes'
	qui gen order=.
	local i=1
	foreach var of local attributes {
		qui replace attribute="`var'" in `i'
		qui replace order=`i' in `i'
		local i=`i'+1	
	}
	qui save `attributes_in_order', replace	
restore

qui ds `attributes', has(type string)
local stringattributevariables="`r(varlist)'"

qui ds `attributes', has(type numeric)
local numericattributevariables="`r(varlist)'"

foreach var of local stringattributevariables {
	qui encode `var', generate(`var'_num)
	drop `var'
	rename `var'_num `var'
	}
	
order `numericattributevariables' `stringattributevariables'

if "`asis'"=="asis" {
	local asdummies="`stringattributevariables'"
	local ascontinuous ="`numericattributevariables'"
	}
else {
	local asdummies="`stringattributevariables' `numericattributevariables'"
	local ascontinuous =""
}

local holdout_original `holdout'

if "`holdout'"=="" {
	 gen holdoutvar=0
	 local holdout=holdoutvar
}

estimates drop _all
foreach id of local ratings {
 	qui reg `id' i.(`asdummies') `ascontinuous' if `holdout'==0
	qui estimates store pw`id'
	qui predict pred_`id'
	qui drop _est_pw`id'
}

local holdout `holdout_original'

capture drop holdoutvar
if `"`savepred'"'~="" & `"`savepred'"'~=`""""'{
	save `savepred', replace
}

qui estout pw* using `partworths', cells(b(fmt(%9.3f))) collabels(, none) replace drop(_cons)

local k=1
foreach var of local ratings {
	rename `var' tempname`k'
	rename pred_`var' pred_tempname`k'
	local k=`k'+1
	local raternames `raternames' `var'
	}

gen profile_id=sum(1)
qui reshape long tempname pred_tempname, i(profile_id) j(rater_id)
qui sum rater_id

qui gen str rater=""
foreach raterid of numlist 1/`r(max)' {
	local ratertemp: word `raterid' of `raternames'
	qui replace rater="`ratertemp'" if rater_id==`raterid'
}
drop rater_id
rename rater rater_id 	

rename tempname rating
rename pred_tempname pred_rating
local savepred_noquotes1: subinstr local savepred `"""' `""' , all 
local savepred_noquotes2: subinstr local savepred_noquotes1 `".dta"' `""' , all 
order profile_id rater_id

if `"`savepred'"'~="" & `"`savepred'"'~=`""""'{
	save "`savepred_noquotes2'_stacked.dta", replace
}

qui levelsof rater_id, local(raterlist)
if "`holdout'"=="" | ("`holdout'"~="" & `num_holdout_obs'<=1){
	qui gen correlation0=.
	foreach r of local raterlist {
		qui correlate rating pred_rating if rater_id=="`r'"
		qui replace correlation0=`r(rho)' if rater_id=="`r'" 
	}
	qui egen temp0=min(correlation0), by(rater_id)
	qui replace correlation0=temp0
	drop temp*
	qui egen rater_tag=tag(rater_id)
	qui keep if rater_tag==1
	qui keep correlation0 rater_id
	qui summarize correlation0 
	local corr0="`r(mean)'"
	}
else if `num_holdout_obs'>1 {
	qui gen correlation0=.
	qui gen correlation1=.
	foreach r of local raterlist {
		qui correlate rating pred_rating if rater_id=="`r'" & `holdout'==0
		qui replace correlation0=`r(rho)' if rater_id=="`r'" & `holdout'==0
		qui correlate rating pred_rating if rater_id=="`r'" & `holdout'==1
		qui replace correlation1=`r(rho)' if rater_id=="`r'" & `holdout'==1
	}
	qui egen temp0=min(correlation0), by(rater_id)
	qui replace correlation0=temp0
	qui egen temp1=min(correlation1), by(rater_id)
	qui replace correlation1=temp1
	drop temp*
	qui egen rater_tag=tag(rater_id)
	qui keep if rater_tag==1
	keep correlation0 correlation1 rater_id
	qui summarize correlation0 
	local corr0=`r(mean)'
	qui summarize correlation1
	local corr1="`r(mean)'"
	}

qui insheet using `partworths', clear
qui gen attribute=regexr(v1,"[0-9bo]*\.","")
qui gen temp1=regexr(v1,attribute,"")
qui gen temp2=regexr(temp1,"[bo]*\.","")
qui gen casetemp=real(temp2)

sort attribute
qui merge m:1 attribute using `attributes_in_order', assert(3)

sort order casetemp
drop _merge order


qui gen attr_level=""

foreach var of local stringattributevariables {
	label values case `var'_num
	qui decode casetemp if attribute=="`var'", gen(`var'case)
	qui replace attr_level=`var'case if attribute=="`var'"
	drop `var'case
	}

foreach var of local numericattributevariables {
	qui replace attr_level=temp2 if attribute=="`var'"
	}

qui gen temp3=subinstr(attr_level," ","_",.)
qui replace attr_level=temp3

drop casetemp temp* v1	
order attribute attr_level

* qui gen temp= proper(attribute)+"_"+attr_level if attr_level~=""
* qui replace temp= proper(attribute) if attr_level==""
qui gen temp=attribute+"_"+attr_level if attr_level~=""
qui replace temp=attribute if attr_level==""
drop attribute attr_level
rename temp attribute
order attribute
qui levelsof attribute, local(attribute) 

local i=1
foreach rating of local ratings {
	rename pw`rating' pw`i'
	local i=`i'+1
	}

foreach level of numlist 1/`=_N' {
	local attribute_ordered `attribute_ordered' `=attribute[`level']'
}
qui reshape long pw, i(attribute) j(id)
qui reshape wide pw, i(id) j(attribute) string

foreach var of local attribute {
	qui rename pw`var' `var' 
	}

order `attribute_ordered'

qui gen id2=""
local i=1
foreach rating of local ratings {
	qui replace id2="`rating'" if id==`i'
	local i=`i'+1
	}
drop id 
rename id2 rater_id
order rater_id

if `"`savepw'"'~="" {
	save `savepw', replace
	}

display ""
if "`corr0'"~=""  {
display "Average of intra-rater correlations (estimation profiles):"%9.2f `corr0'
}
if "`corr0'"==""  {
display "Too few estimation profiles profiles to compute average of intra-rater correlations."
}

if "`holdout'"~="" & `num_holdout_obs'>1 & "`corr1'"~="" {
display "Average of intra-rater correlations (holdout profiles):"%9.2f `corr1'
}

if ("`holdout'"~="" & `num_holdout_obs'<=1) {
display "Too few holdout profiles to compute average of intra-rater correlations."
} 
else if ("`holdout'"~="" & "`corr1'"=="") {
display "All intra-rater correlations are undefined -- check holdout profile ratings."
}

end

