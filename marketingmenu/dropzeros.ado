program define dropzeros
version 10

foreach var of varlist * {
	qui sum `var'
	if "`r(min)'"=="0" & "`r(max)'"=="0" {
		drop `var'
	}
}
end
