*!  VERSION 0.2  10dec2014 (replaced calibration with training language and added AUC output)
*!  Florian Zettelmeyer

program define gains

syntax varlist(max=10) [if], DEPvar(varname numeric) [NQuantiles(integer 10) TRAINvar(varname numeric) only(string) name(string)]

tokenize `varlist'
marksample touse
tempvar trainvaruse aucvar aucxtile
local i=0

*** Check wether training variables of dummy 
if "`trainvar'"~="" {
	qui levelsof `trainvar', local(trainvarvalues)
	foreach level of local trainvarvalues {
		if `level'~=0 & `level'~=1 {
			display as error `"The training dummy variable "`trainvar'" specified in"' 
			display as error "trainvar() contains values other than 0 or 1"
			error 198			
		}
	}
}

*** Set the right sample selection variables for only() and trainvar() options
if "`only'"=="" {
	if "`trainvar'"=="" {
		local samplelist="1"
		gen `trainvaruse'=1
	}
	else {
		local samplelist="1 0"
		gen `trainvaruse'=`trainvar'
	}
}
else if "`only'"=="validation" {
	if "`trainvar'"=="" {
		display as error "To display the validation results you need to specify a" 
		display as error "training dummy variable using the trainvar() option; "
		error 198
	}
	else {
		local samplelist="0"
		gen `trainvaruse'=`trainvar'
	}
}
else if "`only'"=="training" {
	if "`trainvar'"=="" {
		display as error "To display the training results you need to specify a" 
		display as error "training dummy variable using the trainvar() option; "
		error 198
	}
	else {
		local samplelist="1"
		gen `trainvaruse'=`trainvar'
	}
}
else {
	display as error `"The only() option must contain "validation" or "training" or "both""'
	error 198
}

*** Create quantiles and chart datasets
foreach predvar of local varlist {
	local i=`i'+1
	foreach v of numlist `samplelist' {
		preserve
		qui keep if `touse' & `trainvaruse'==`v'
		qui des
		local samplesize =`r(N)'

		* check that sample is large enough for xtile command
		if `samplesize'<`nquantiles'+1 {
			if `v'==1 local samplename="training"
			else local samplename="validation"
			display as error `"The `samplename' sample specified in "`trainvar'" has `samplesize' observations."'
			display as error `"This is too small to calculate gains with `nquantiles' quantiles;"'
			error 198
		}
		
		qui xtile `aucxtile'=`predvar', nquantiles(100)
		qui roctab `depvar' `aucxtile'
		local aucvar=r(area)
	
		qui xtile dec=`predvar', nquantiles(`nquantiles')
		qui replace dec=`nquantiles'+1-dec
		collapse (count) customernum=`depvar' (sum) buyernum=`depvar', by(dec)
		gen cumcustomers=sum(customernum)
		gen cumbuyers=sum(buyernum)

		egen customertotal=sum(customernum)
		egen buyertotal=sum(buyernum)

		gen cumpcustomers=cumcustomers/customertotal
		gen pbuyers=cumbuyers/buyertotal

		qui des
		local nobs =`r(N)'+1
		qui set obs `nobs'
		qui replace cumpcustomers=0 if cumpcustomers==.
		qui replace pbuyers=0 if pbuyers==.
		qui replace dec=0 if dec==.
	
		qui replace cumpcustomers=cumpcustomers*100
		qui replace pbuyers=pbuyers*100
	
		rename pbuyers pb_`i'_`v'
		rename cumpcustomers cpc_`i'_`v'

		display as result ""
		if "`trainvar'"=="" {
			label var pb_`i'_`v' "`predvar'"
			display as result "Results for `predvar' (sample of `samplesize' customers):"
		    display as text "AUC:" as result %6.3f `aucvar' as text "  (area under curve from ROC curve)"
		}
		else {
			if `v'==1 {
			label var pb_`i'_`v' "`predvar' (train)"
			display as result "Results for `predvar' (training sample of `samplesize' customers):"
		    display as text "AUC:" as result %6.3f `aucvar' as text "  (area under curve from ROC curve)"
			}
			else {
				label var pb_`i'_`v' "`predvar' (val)"
				local msymbol="msymbol(diamond)"
				display as result "Results for `predvar' (validation sample of `samplesize' customers):"
			    display as text "AUC:" as result %6.3f `aucvar' as text "  (area under curve from ROC curve)"
			}
		}
		
		keep dec cpc_`i'_`v' pb_`i'_`v'
		sort dec
		tempfile data_`i'_`v'
		qui save `data_`i'_`v'', replace

		qui drop if dec==0	
		label var dec "Quantile"
		label var pb_`i'_`v' "Cum. % Responses"
		label var cpc_`i'_`v' "Cum. % Cust."
		tabdisp dec, cellvar(cpc_`i'_`v' pb_`i'_`v') format(%8.1f) center

		restore
		local graphlist_1 = "`graphlist_1' (con pb_`i'_`v' cpc_`i'_`v', sort `msymbol')" 
	}
}

preserve

local numprevars: list sizeof varlist
local first: word 1 of `samplelist'

foreach v of numlist `samplelist' {
	foreach predvarnum of numlist 1/`numprevars' {
			if `predvarnum'==1 & `v'==`first' {
				use `data_1_`first'', clear
			}
			else {
				qui merge 1:1 dec using `data_`predvarnum'_`v''
				drop _merge
			}
	}
}

qui des
local nobs =`r(N)'-1
gen pbuyerbase=(sum(1/`nobs')-1/`nobs')*100
gen pcustomerbase=pbuyerbase

label var pbuyerbase `"no model"'

*** Probe whether a name is specified and put in a option if needed
if "`name'"=="" {
	local nameoption=""
	}
	else {
	local nameoption="name(`name')"
}

twoway `graphlist_1' `graphlist_0'  (connected pbuyerbase pcustomerbase, sort msymbol(none) lcolor(black)), xtitle("% of Customers Targeted") ytitle("% of Responses") legend( col(2) size(vsmall) ) aspect(.8) ylabel(#10) title(Cumulative Gains Chart) `nameoption'

restore
end
