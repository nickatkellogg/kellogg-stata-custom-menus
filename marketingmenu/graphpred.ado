program define graphpred
syntax varlist(max=1) [if], DEPvar(varname numeric) [Revenue(real -999999) Cost(real -999999) Qvar(varname numeric) ADjustprob(real 1) NQuantiles(integer 30) name(string) noSCore]

set more off
tokenize `varlist'
marksample touse
local sname="`1'"

if (`revenue'==-999999 & `cost'~=-999999) | (`revenue'!=-999999 & `cost'==-999999) {
	display as error "Revenue/cost cannot be specified without each other"
	error 198			
	}

if "`name'"=="" {
	local name="`1'"
	}

if "`qvar'"=="" {
	local qvar="q_`sname'"
	capture drop `qvar'
	xtile `qvar'=`1' if `touse', nquantile(`nquantiles') 
	qui replace `qvar'=`nquantiles'+1-`qvar' if `touse'
	local newqvar=1
	}

preserve
qui keep if `touse'
foreach vname in av avdepvar profit tag nc nb min max cut {
	capture drop `vname'
	}
qui egen tag=tag(`qvar')
qui egen nc=count(1), by(`qvar') // nc = number of customers
qui egen nb=sum(`depvar'), by(`qvar') // nb = number of buyers
qui egen avdepvar=mean(`depvar'), by(`qvar')
qui replace avdepvar=avdepvar*`adjustprob'
qui egen av=mean(`1'), by(`qvar')
qui egen min=min(`1'), by(`qvar')
qui egen max=max(`1'), by(`qvar')
qui keep if tag
sort `qvar'

qui gen cut= (min+max[_n+1])/2
qui replace cut= min if cut==.

qui gen cc=sum(nc) // cc = cummulative number of customers
qui gen cb=sum(nb) // cb = cummulative number of buyers
qui gen cum_av_`depvar'=(cb/cc)*`adjustprob'

label var `qvar' "Quantile of `sname'"
label var cut "Score cut-off"
label var av "Predicted (`sname')"
label var avdepvar "Actual (av. of `depvar')"
label var cc "Cum. # Customers"
label var cum_av_`depvar' "Cummulative (cum. av. of `depvar')"
format %9.3g av avdepvar cum_av_`depvar' 

if (`revenue'~=-999999 & `cost'~=-999999) {
	local be=`cost'/`revenue'
	local yline=`"(line be `qvar', msymbol(none) lcolor(red) sort) (scatter profit `qvar', msymbol(none) connect(direct) lcolor(green) yaxis(2) ytitle("Cummulative Profit", axis(2)) sort)"'
	local yline2="yline(`be', lcolor(red))"
	gen be=`be'
	label var be "Break-even"
	gen profit=cum_av_`depvar'*cc*`revenue'-cc*`cost'
	label var profit "Cum. Profit"
	format %9.3g profit  
	local profit="profit"
	}

tabdisp `qvar', cellvar(cut cum_av_`depvar' cc  `profit')  center

if "`score'"=="noscore" {
			local plotscore=""
		}
		else {
			local plotscore="(scatter av `qvar', msymbol(none) connect(direct) lcolor(blue) sort)"
			}

twoway (bar avdepvar `qvar', lcolor(gray) lwidth(thin) fcolor(none) sort ytitle("Probability") xlabel(#10)) `plotscore' `yline' , name(`name', replace) `yline2'

end


