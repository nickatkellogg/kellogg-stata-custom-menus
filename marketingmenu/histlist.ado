program define histlist

syntax varlist [, *]

foreach var of varlist `varlist' {
	qui histogram `var', name(`var', replace) nodraw `options'
	}

graph combine `varlist', scale(.8)

end
