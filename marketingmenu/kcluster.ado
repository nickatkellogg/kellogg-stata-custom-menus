program define kcluster

syntax varlist (min=1), K(integer) [Name(string) Replace] [raw]
preserve
if `k'<2 {
	display as error `"k>=2 required;"'
	error 198
}

* Identifies constant variables 
local flag=0
foreach var of local varlist {
	qui sum `var'
	if "`r(min)'"=="`r(max)'" {
		display `""`var'" is a constant"'
		local flag=`flag'+1
	}
}
if `flag'>0 {
	display "Please rerun kcluster without constant variable(s)"
	error 198
}

* Standardize variables
foreach var of local varlist {
	tempvar std`var'
	if "`raw'"=="" {
		qui egen std`var'=std(`var')
		rename `var' orig`var'
		rename std`var' `var'
	}
}

if "`replace'"=="replace" {
	capture cluster delete `name', zap
}

tempname tmp1 tmp2

cluster wardslinkage `varlist', name(`tmp1') measure(L2squared)
cluster generate `tmp2'=groups(`k'), name(`tmp1')
capture cluster delete `tmp1', zap
cluster kmeans `varlist', k(`k') start(group(`tmp2')) name(`name') measure(L2)

local clusterlist: char _dta[_cluster_objects]
local name: word 1 of `clusterlist'

local oldclusterlist: char _dta[clusterids]
char _dta[clusterids] `name' `oldclusterlist'

capture drop N
gen N=_N
tabstat N, stat(count) by(`name') nototal

foreach var of local varlist {
	if "`raw'"=="" {
		drop `var'
		rename orig`var' `var' 
	}
}

tabstat `varlist', stat(mean) by(`name') nototal
capture drop N
restore, not
end
