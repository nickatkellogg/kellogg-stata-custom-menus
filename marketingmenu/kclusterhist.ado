*! 1-29-2011
*! Florian Zettelmeyer

program kclusterhist
version 11
syntax [varlist(default=none)], [CLustervar(varname numeric)]
tokenize `varlist'

if "`clustervar'"=="" {
	local clusterlist: char _dta[_cluster_objects]
	local clustervar: word 1 of `clusterlist'
	if "`clustervar'"=="" {
		display as error `"Please run a cluster analysis before performing a post-cluster histogram diagnostic;"'
		error 301  
	}
}

if "`varlist'"=="" {
	local varlist: char _dta[`clustervar'_vars]
}

* Find number of variables; important for looping
local maxvar: list sizeof varlist
tokenize `varlist'

if `maxvar'>3 {
	display "Plotting cluster histogram diagnostic on `maxvar' variables. This command might take a little while ... "
	}

quietly levelsof `clustervar', local(clusterlevels)
* Find number of cluster levels; only used for graph name
local numcluster: list sizeof clusterlevels

foreach v1 of varlist `varlist' { 
	local varlabel: variable label `v1'
	if "`varlabel'"=="" {
		local varlabel="`v1'"
	}
	local i=0
	local legendcombine=""
	capture drop kdensitysupport
	kdensitysupport `v1'
	qui sum `clustervar'
	scalar denom=r(N)
	foreach cluster of local clusterlevels {
			qui sum `clustervar' if `clustervar' ==`cluster'
			scalar numer=r(N)
			scalar frac=numer/denom
			tempvar tmpx_`cluster' tmpy_`cluster' zero1 middle1 zero2 middle2 drop1 drop2 sortorder
			kdensity `v1' if `clustervar' ==`cluster', at(kdensitysupport) generate(`tmpx_`cluster'' `tmpy_`cluster'') nograph
			qui replace `tmpy_`cluster''=`tmpy_`cluster''*frac
			* Here we trim off the tails of the distribution at zero density
			gen `sortorder'=sum(1) // remember the sortorder
			sort `tmpx_`cluster''
			gen `zero1'=`tmpy_`cluster''[_n]~=`tmpy_`cluster''[_n+1]
			gen `middle1'=sum(`zero1')
			gen `drop1'=(`zero1'==0 & `middle1'==0)
			gsort -`tmpx_`cluster''
			gen `zero2'=`tmpy_`cluster''[_n]~=`tmpy_`cluster''[_n+1]
			gen `middle2'=sum(`zero2')
			gen `drop2'=(`zero2'==0 & `middle2'==0)
			qui replace `tmpy_`cluster''=. if `drop1' | `drop2'
			qui replace `tmpx_`cluster''=. if `drop1' | `drop2'
			sort `sortorder'
			
			* Assemble the graph command
			local graphmacro`v1' `"`graphmacro`v1'' (line `tmpy_`cluster'' `tmpx_`cluster'', sort) "'
			local i=`i'+1
			local legendcombine `"`legendcombine' `i' "`clustervar'=`cluster'""'
			}
	capture drop kdensitysupport
	twoway `graphmacro`v1'', legend(off order(`legendcombine')) name(graph`v1', replace)  xtitle("`varlabel'") ytitle("Density") nodraw
	local graphcombine `"`graphcombine' graph`v1'"'
	foreach cluster of local clusterlevels {
		drop `tmpx_`cluster'' `tmpy_`cluster''
	}
	}

grc1leg `graphcombine', title(Cluster Histogram Diagnostic for `clustervar' (k=`numcluster')) name(`clustervar', replace) scale(.8) legendfrom(graph`1') 

end

