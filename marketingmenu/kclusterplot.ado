*! 1-15-2011
*! Florian Zettelmeyer

program kclusterplot
version 11
syntax [varlist(default=none)], [CLustervar(varname numeric)]

if "`varlist'"=="" & "`clustervar'"=="" {
	local clusterlist: char _dta[_cluster_objects]
	local clustervar: word 1 of `clusterlist'
	if "`clustervar'"=="" {
		display as error `"Please run a cluster analysis before performing a post-cluster pairwise diagnostic;"'
		error 301  
	}
	local varlist: char _dta[`clustervar'_vars]
}
else if ("`varlist'"=="" & "`clustervar'"~="") | ("`varlist'"~="" & "`clustervar'"=="") {
	display as error `"To run the post-cluster pairwise diagnostic on the last cluster command, please type "kclusterplot" without arguments."'  
	display as error `"Otherwise please specify: "kclusterplot {it:varlist}, clustervar({it:clustervar})""'
	error 198
}

* Find number of variables; important for looping
local maxvar: list sizeof varlist
tokenize `varlist'

if `maxvar'>3 {
	display "Plotting pairwise cluster diagnostic on `maxvar' variables. This command might take a little while ... "
	}

quietly levelsof `clustervar', local(clusterlevels)
* Find number of cluster levels; only used for graph name
local numcluster: list sizeof clusterlevels

forvalues v1=1/`maxvar' { 
	local v1plus1=`v1'+1
	forvalues v2=`v1plus1'/`maxvar' { 
		local i=0
		local legendcombine=""
		foreach cluster of local clusterlevels {
			local i=`i'+1
			local legendcombine `"`legendcombine' `i' "`clustervar'=`cluster'""'
 			local graphmacro`v1'`v2' `"`graphmacro`v1'`v2'' || scatter ``v2'' ``v1'' if `clustervar' ==`cluster'"'
			twoway `graphmacro`v1'`v2'', legend(off order(`legendcombine')) name(graph`v1'`v2', replace) nodraw  aspectratio(.7)
 			}
		local graphcombine `"`graphcombine' graph`v1'`v2'"'
		}
	}

* The next 10 lines are only there to establish the blanks in the combined graph
local maxvarm1=`maxvar'-1
local maxvarm2=`maxvar'-2
local maxvarm1sq =`maxvarm1'*`maxvarm1'
forvalues v1=1/`maxvarm2' { 
	local start`v1'=`v1'+`v1'*`maxvarm1'
	}
forvalues v1=1/`maxvarm2' { 
	local numcombine `"`numcombine' `start`v1''(`maxvarm1')`maxvarm1sq'"'
	}
capture numlist "`numcombine'", sort // capture is important to make sure that even for only 2 variable we can do graph combine.

if "`numcombine'"~="" {
	grc1leg `graphcombine', scale(.8) colfirst title(Cluster Analysis Pairwise Diagnostic for `clustervar' (k=`numcluster')) holes(`r(numlist)')  cols(`maxvarm1')  name(`clustervar', replace)  legendfrom(graph12)  imargin(small)
	}
else {
	grc1leg `graphcombine', scale(.8) colfirst title(Cluster Analysis Pairwise Diagnostic for `clustervar' (k=`numcluster'))  cols(`maxvarm1')  name(`clustervar', replace)  legendfrom(graph12) imargin(small)
}
end
