program define kdendro

syntax varlist [, raw]
preserve

* Identifies constant variables 
local flag=0
foreach var of local varlist {
	qui sum `var'
	if "`r(min)'"=="`r(max)'" {
		display `""`var'" is a constant"'
		local flag=`flag'+1
	}
}
if `flag'>0 {
	display "Please rerun kdendro without constant variable(s)"
	error 198
}

* Standardize variables
foreach var of local varlist {
	tempvar std`var'
	if "`raw'"=="" {
		qui egen std`var'=std(`var')
		rename `var' orig`var'
		rename std`var' `var'
	}
}

tempname tmp1 tmp2

qui cluster wardslinkage `varlist', measure(L2squared)
local clusterlist: char _dta[_cluster_objects]
local name: word 1 of `clusterlist'
qui sum `name'_id
scalar nobs=r(N)

scalar cutnumber=min(30,r(N))
local cutnumberlocal=cutnumber
capture cluster dendrogram `name', cutnumber(`cutnumberlocal') showcount xlabel(,labsize(*.5)) title("Dendrogram for cluster analysis")

scalar orig=cutnumber
scalar i=1 
scalar j=1

while _rc==198 {
	scalar tempcut=orig - i*j
	local cutnumberlocal=tempcut
	scalar tempcut0=orig - i*abs(j)
	scalar tempcut1=orig + i*abs(j)
	if tempcut1>nobs & tempcut0<2 {
		display as error "Cannot display dendrogram due to ties in the dataset;"
		error 198
	}
	if tempcut>=2 capture cluster dendrogram `name', cutnumber(`cutnumberlocal') showcount xlabel(,labsize(*.5)) title("Dendrogram for cluster analysis")
	scalar j=-j
	if j==1 scalar i=i+1
	}

capture cluster delete `name', zap

foreach var of local varlist {
	if "`raw'"=="" {
		drop `var'
		rename orig`var' `var' 
	}
}
restore, not
end
