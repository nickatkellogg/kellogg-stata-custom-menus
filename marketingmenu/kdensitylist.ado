program define kdensitylist

syntax varlist [, *]

foreach var of varlist `varlist' {
	qui kdensity `var', note("") title("") name(`var', replace) nodraw `options'
	}

graph combine `varlist', scale(.8)

end
