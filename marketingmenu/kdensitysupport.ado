program define kdensitysupport

	syntax varlist(max=1)

	qui des
	local n=`r(N)'
	qui sum `varlist', det

	/*default bandwidth calculation */
	
	local r=r(max)-r(min)
	local wwidth = min( r(sd) , (r(p75)-r(p25))/1.349)
	if `wwidth' <= 0.0 {
		local wwidth = r(sd)	
	}
	local wwidth = 0.9*`wwidth'/(r(N)^.20)
	scalar delta = (r(max)-r(min)+2*`r'/5)/(`n'-1)
	qui gen kdensitysupport = r(min)-`r'/5+(_n-1)* delta in 1/`n'
end
