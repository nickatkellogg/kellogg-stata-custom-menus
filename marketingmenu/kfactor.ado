program define kfactor
    version 10
	syntax varlist(numeric), rotation(string) [factors(integer 99999)]
	if "`rotation'"~="varimax" & "`rotation'"~="none" {
		display as error `"Rotation options are "varimax" or "none""'
		error 198
	}
	
	if "`factors'"=="99999" {
		local factorpass=""
		local eigen "mineigen(1)"
	}
	else {
		local factorpass "factors(`factors')"
		local eigen "mineigen(0)"
	}
 	kfactor_command `varlist', `factorpass' `eigen'
	if "`rotation'"=="varimax" {
		quietly rotate, varimax normalize
	}
	ksortl 
	capture drop fs*
	quietly predict fs*, regression
	matrix define flmatrix=e(r_L)
	if `e(f)'==2 {
			loadingplot, yline(0, lstyle(grid) lw(*.7) lcolor(gs0)) xline(0, lstyle(grid) lw(*.7) lcolor(gs0)) 
		}
end
