program define kprefactor
    version 10
	syntax varlist(min=2 numeric) [if] [in]
    factortest `varlist' `if' `in'
    kfactor_eigen `varlist' `if' `in' // default is pcf
    scree,  title(Scree Plot of Eigenvalues) yline(1)
end

