program define kregdiag
	version 10

	display "Plotting regression diagnostic for regression with `e(df_m)' independent variables."
	display "This command will produce 3 separate graph windows and might take a little while ..."

	tempname kregdiag1 kregdiag2 kregdiag3 kregdiag4 kregdiag5 kregdiag6 
	
	kyvfplot, name(`kregdiag1', replace) nodraw
	krvfplot, name(`kregdiag2', replace)  nodraw
	krvoplot, name(`kregdiag3', replace) nodraw
	kqqres, name(`kregdiag4', replace) nodraw
	*krvlplot, name(`kregdiag5', replace) nodraw
	*kcooklevplot, name(`kregdiag6', replace) nodraw
	*graph combine `kregdiag1' `kregdiag2' `kregdiag3' `kregdiag4' `kregdiag5' `kregdiag6', name(kregdiag1, replace) 
	graph combine `kregdiag1' `kregdiag2' `kregdiag3' `kregdiag4', name(kregdiag1, replace) 

	krvpplots, name(kregdiag2, replace) title(Residual vs. Predictor)

	avplots, rlopts(lcolor(gs10) lpattern(dash)) name(kregdiag3, replace)  title(Leverage Plots)
	*kavplots, rlopts(lcolor(gs10) lpattern(dash)) name(kregdiag3, replace)  title(Leverage Plots)
end


program define kyvfplot	/* actual vs. fitted; modifies rvfplot to be yvf and adds lowess smooth */
	version 6
	if _caller() < 8 {
		rvfplot_7 `0'
		exit
	}

	_isfit cons anovaok
	syntax [, * ]
	
	_get_gropts , graphopts(`options') getallowed(plot addplot)
	local options `"`s(graphopts)'"'
	local plot `"`s(plot)'"'
	local addplot `"`s(addplot)'"'

	tempvar hat y 
	quietly _predict `hat' if e(sample)
	quietly gen `y' = `e(depvar)' if e(sample)
	
	lowess  `y' `hat', gen(tempsmooth) nograph
	
	qui gen maxy_yhat=max(`y', `hat', tempsmooth)
	qui gen miny_yhat=min(`y', `hat', tempsmooth)
	qui sum maxy_yhat
	local mymax=`r(max)'
	qui sum miny_yhat
	local mymin=`r(min)'
	drop maxy_yhat miny_yhat tempsmooth
	 	
	version 8: graph twoway		///
	(scatter `y' `hat',		///
		`options'  	///
	)				///
	(lowess  `y' `hat',		///
		note("") 	///
	)				///
	(function y=x, range(`mymin' `mymax') lcolor(gs10) lpattern(dash) ///
	), legend(off) title(Actual vs. Fitted) xtitle(Fitted values) ytitle(Actual values) ///
	|| `plot' || `addplot'		///
	// blank
end


program define krvfplot	/* residual vs. fitted; modifies rvfplot to add lowess smooth */
	version 6
	if _caller() < 8 {
		rvfplot_7 `0'
		exit
	}

	_isfit cons anovaok
	syntax [, * ]

	_get_gropts , graphopts(`options') getallowed(plot addplot)
	local options `"`s(graphopts)'"'
	local plot `"`s(plot)'"'
	local addplot `"`s(addplot)'"'

	tempvar resid hat
	quietly _predict `resid' if e(sample), resid
	quietly _predict `hat' if e(sample)
	version 8: graph twoway		///
	(scatter `resid' `hat',		///
		`options'		///
	)				///
	(lowess  `resid' `hat',		///
		note("")		///
	), legend(off) title(Residual vs. Fitted) yline(0, lcolor(gs10) lpattern(dash))	xtitle(Fitted values) ytitle(Residuals)			///
	|| `plot' || `addplot'		///
	// blank
end


program define krvoplot	/* residual vs. row order; modifies rvfplot to be yvo and adds lowess smooth */
	version 6
	if _caller() < 8 {
		rvfplot_7 `0'
		exit
	}

	_isfit cons anovaok
	syntax [, * ]

	_get_gropts , graphopts(`options') getallowed(plot addplot)
	local options `"`s(graphopts)'"'
	local plot `"`s(plot)'"'
	local addplot `"`s(addplot)'"'

	tempvar resid ord
	quietly _predict `resid' if e(sample), resid
	quietly gen `ord'=_n if e(sample)
	version 8: graph twoway		///
	(scatter `resid' `ord',		///
		`options'		///
	)				///
	(lowess  `resid' `ord',		///
		note("")		///
	), legend(off) title(Residual vs. Row Order) yline(0, lcolor(gs10) lpattern(dash)) xtitle(Row order) ytitle(Residuals) 			///
	|| `plot' || `addplot'		///
	// blank
end


program define kqqres	/* std. residual normal quantile plot; coded from scratch */
	version 9
	syntax [, * ]
	tempvar sres
	quietly _predict `sres' if e(sample), rstandard
	qnorm `sres', title(Normal Q-Q Plot) xtitle(Theoretical quantiles) rlopts(lcolor(gs10) lpattern(dash)) `options'
end


program define krvlplot /* std res vs leverage; modifies lvr2plot to be rvl and adds lowess smooth */
	version 6
	if _caller() < 8 {
		lvr2plot_7 `0'
		exit
	}

	_isfit cons anovaok
	syntax [, * ]

	_get_gropts , graphopts(`options') getallowed(plot addplot)
	local options `"`s(graphopts)'"'
	local plot `"`s(plot)'"'
	local addplot `"`s(addplot)'"'

	if "`e(vcetype)'"=="Robust" { 
		di in red "leverage plot not available after robust estimation"
		exit 198
	}

	tempvar h r 
	quietly { 
		_predict `h' if e(sample), hat
		_predict `r' if e(sample), rstandard
	}

	label var `h' "Leverage"
	local xttl : var label `h'
	label var `r' "Standardized Residuals"
	local yttl : var label `r'
	version 8: graph twoway		///
	(scatter `r' `h',		///
		ytitle(`"`yttl'"')	///
		xtitle(`"`xttl'"')	///
		yline(0, lcolor(gs10) lpattern(dash))		///
		xline(0, lcolor(gs10) lpattern(dash))		///
		`options'		///
	)				///
	(lowess `r' `h'), legend(off) title(Residuals vs. Leverage) ///
	|| `plot' || `addplot'		///
	// blank
end


program define kcooklevplot /* cook's distance vs leverage;  modifies lvr2plot to be cook vs lev and adds lowess smooth */
	version 6
	if _caller() < 8 {
		lvr2plot_7 `0'
		exit
	}

	_isfit cons anovaok
	syntax [, * ]

	_get_gropts , graphopts(`options') getallowed(plot addplot)
	local options `"`s(graphopts)'"'
	local plot `"`s(plot)'"'
	local addplot `"`s(addplot)'"'

	if "`e(vcetype)'"=="Robust" { 
		di in red "leverage plot not available after robust estimation"
		exit 198
	}

	tempvar h cook 
	quietly { 
		_predict `h' if e(sample), hat
		_predict `cook' if e(sample), cooksd
	}

	label var `h' "Leverage"
	local xttl : var label `h'
	label var `cook' "Cook's Distance"
	local yttl : var label `cook'
	version 8: graph twoway		///
	(scatter `cook' `h',		///
		ytitle(`"`yttl'"')	///
		xtitle(`"`xttl'"')	///
		`options'		///
	)				///
	(lowess `cook' `h') , legend(off)  title(Cook's Distance vs. Leverage) ///
	|| `plot' || `addplot'		///
	// blank
end


program define krvpplot, rclass sort  /* residual vs. predictor; based heavily off avplot with some rvpplot; adds lowess smooth */
	local vv : display "version " string(_caller()) ", missing:"
	version 9
	
	_isfit cons newanovaok
	_ms_op_info e(b)
	local fvops = r(fvops)
	local tsops = r(tsops)
	if `fvops' {
		if _caller() < 11 {
			local vv "version 11:"
		}
	}

	syntax [anything(name=var id="varname")] [, *]

	if `"`var'"' == "" {
		syntax varname [, *]
		exit 100
	}
	if `:list sizeof var' > 1 {
		error 103
	}
	capture _ms_extract_varlist `var'
	if !c(rc) {
		local varlist `"`r(varlist)'"'
		if `:list sizeof varlist' > 1 {
			error 103
		}
		if _b[`varlist'] == 0 {
			di in gr "(`varlist' was dropped from model)"
			exit 399
		}
	}
	else {
		capture _msparse `var'
		if c(rc) {
			error 198
		}
		local varlist `"`r(stripe)'"'
	}
	
	_get_gropts , graphopts(`options') getallowed(RLOPts plot addplot)
	local options `"`s(graphopts)'"'
	local rlopts `"`s(rlopts)'"'
	local plot `"`s(plot)'"'
	local addplot `"`s(addplot)'"'
	_check4gropts rlopts, opt(`rlopts')
		
	local v `varlist'
	if "`e(depvar)'"=="`v'" { 
		di in red "cannot include dependent variable"
		exit 398
	}
	/* The section from here through the if/else comes from rvpplot. It can be omitted if you
	   do not wish to exclude non-predictor variables from the x-axis of a krvpplot. */
	if "`e(cmd)'" == "anova" {
		anova_terms
		local aterms `r(rhs)'
		local found 0
		foreach trm of local aterms {
			if "`trm'" == "`v'" {
				local found 1
				continue, break
			}
		}
		if !`found' {
			di in red "`v' is not in the model"
			exit 398
		}
	}
	else { /* regress */
		capture local beta=_b[`v']
		if _rc {
			di in red "`v' is not in the model"
			exit 398
		}
	}
	
	_ms_parse_parts `v'
	local isvar = r(type) == "variable"
	local hasts = "`r(ts_op)'" != ""
	if `isvar' {
		local x : copy local v
	}
	else {
		fvrevar `v'
		local x `r(varlist)'
	}	

	tempvar touse resid
	gen byte `touse' = e(sample)
	quietly _predict `resid' if `touse', resid

	local yttl : var label `resid'
	local xttl : var label `x'
	if `"`xttl'"' == "" {
		local xttl `varlist'
	}
	
	if `"`plot'`addplot'"' == "" {
		local legend legend(nodraw)
	}
	version 8: graph twoway		///
	(scatter `resid' `x'		///
		if `touse',		///
		ytitle(`"`yttl'"')	///
		xtitle(`"`xttl'"')	///
		`legend'		///
		`options'		///
	)				///
	(lowess `resid' `x', note("") ///
	),  yline(0, lcolor(gs10) lpattern(dash)) ///
	|| `plot' || `addplot'		///
	// blank
end


program define krvpplots   /* calls krvpplot for each predictor varaible in regression */
	version 9                                                                                   
	local vv : display "version " string(_caller()) ":"

	_isfit cons newanovaok
	syntax [, * ]

	_get_gropts , graphopts(`options') getcombine getallowed(plot addplot)
	local options `"`s(graphopts)'"'
	local gcopts `"`s(combineopts)'"'
	if `"`s(plot)'"' != "" {
		di in red "option plot() not allowed"
		exit 198
	}
	if `"`s(addplot)'"' != "" {
		di in red "option addplot() not allowed"
		exit 198
	}		

	_getrhs rhs
	tokenize `rhs'

	while "`1'"!="" { 
		tempname tname
		local base `names'
		local names `names' `tname'
		capture `vv' krvpplot `1', name(`tname') nodraw `options'
		if _rc { 
			if _rc!=399 {
				exit _rc
			} 
			local names `base'
		}
		mac shift 
	}

	version 8: graph combine `names' , `gcopts'
	version 8: graph drop `names'
end


program define kavplot, rclass sort   /* avplot with loess smooth */
	local vv : display "version " string(_caller()) ", missing:"
	version 6
	if _caller() < 8 {
		avplot_7 `0'
		return add
		exit
	}

	_isfit cons newanovaok
	_ms_op_info e(b)
	local fvops = r(fvops)
	local tsops = r(tsops)
	if `fvops' {
		if _caller() < 11 {
			local vv "version 11:"
		}
	}

	syntax [anything(name=var id="varname")] [, *]

	if `"`var'"' == "" {
		syntax varname [, *]
		exit 100
	}
	if `:list sizeof var' > 1 {
		error 103
	}
	capture _ms_extract_varlist `var'
	if !c(rc) {
		local varlist `"`r(varlist)'"'
		if `:list sizeof varlist' > 1 {
			error 103
		}
		if _b[`varlist'] == 0 {
			di in gr "(`varlist' was dropped from model)"
			exit 399
		}
	}
	else {
		capture _msparse `var'
		if c(rc) {
			error 198
		}
		local varlist `"`r(stripe)'"'
	}

	_get_gropts , graphopts(`options') getallowed(RLOPts plot addplot)
	local options `"`s(graphopts)'"'
	local rlopts `"`s(rlopts)'"'
	local plot `"`s(plot)'"'
	local addplot `"`s(addplot)'"'
	_check4gropts rlopts, opt(`rlopts')

	local v `varlist'
	local wgt "[`e(wtype)' `e(wexp)']"
	tempvar touse resid lest evx hat

			/* determine if v in original varlist	*/
	if "`e(depvar)'"=="`v'" { 
		di in red "cannot include dependent variable"
		exit 398
	}
	local lhs "`e(depvar)'"
	if "`e(vcetype)'"=="Robust" {
		local robust="robust"
	}
	_getrhs rhs
	gen byte `touse' = e(sample)
	if "`e(clustvar)'"~="" {
		tempname myest
		local cluster="cluster(`e(clustvar)')"
		estimates hold `myest'
		`vv' ///
		qui _regress `lhs' `rhs' if `touse', `robust'
		local ddof= e(df_r)
		estimates unhold `myest'
	}
	else {
		local ddof= e(df_r)
	}
	local inorig : list v in rhs
	quietly {
		_predict `resid' if `touse', resid
	}
	_ms_parse_parts `v'
	local isvar = r(type) == "variable"
	local hasts = "`r(ts_op)'" != ""
	if `isvar' {
		local x : copy local v
	}
	else {
		fvrevar `v'
		local x `r(varlist)'
	}
	if !`inorig' {	 		/* not originally in	*/
		capture assert `v'!=. if `touse'
		if _rc { 
			di in red "`v' has missing values" _n /*
		*/ "you must reestimate including `v'"
			exit 398
		}
		estimate hold `lest'
		capture { 
			`vv' ///
			regress `x' `rhs' `wgt' if `touse',		///
				`robust' `cluster'
			_predict `evx' if `touse', resid
			`vv' ///
			regress `resid' `evx' `wgt' if `touse',		///
				`robust' `cluster'
			ret scalar coef = _b[`evx']
			_predict `hat' if `touse'
			`vv' ///
			regress `lhs' `x' `rhs' `wgt' if `touse',	///
				`robust' `cluster'
			ret scalar se = _se[`x']
		}
		local rc=_rc
	}
	else {				/* originally in	*/
		drop `resid'
		if _b[`v']==0 { 
			di in gr "(`v' was dropped from model)"
			exit 399
		}
		estimate hold `lest'
		capture { 
			if `isvar' {
				local RHS : list rhs - v
			}
			else	local RHS : copy local rhs
			if `fvops' {
				fvrevar `RHS'
				local RHS `"`r(varlist)'"'
				local RHS : list RHS - x
			}
			`vv' ///
			regress `lhs' `RHS' `wgt' if `touse',	///
				`robust' `cluster'
			_predict double `resid' if `touse', resid
			`vv' ///
			regress `x' `RHS' `wgt' if `touse',	///
				`robust' `cluster'
			_predict double `evx' if `touse', resid
			`vv' ///
			regress `resid' `evx' `wgt' if `touse',	///
				`robust' `cluster'
			ret scalar coef = _b[`evx']
			local seevx=_se[`evx']
			_predict double `hat' if `touse'
			`vv' ///
			regress `lhs' `rhs' `wgt' if `touse',	///
				`robust' `cluster'
			ret scalar se = _se[`v']
		}
		local rc=_rc
	}
	estimate unhold `lest'
	if `rc' {
		error `rc'
	}

	/* double save in S_# */
	global S_1 = return(coef)
	global S_2 = return(se)

	version 2.1
	local t = round(return(coef)/return(se),.01)
	local coef=return(coef)
	local se=return(se)
	version 6

	if "`robust'"=="robust" {
		local note "coef = `coef', (robust) se = `se', t = `t'" 
	}
	else {
		local note "coef = `coef', se = `se', t = `t'" 
	}
	label var `resid' "e( `lhs' | X )"
	local yttl : var label `resid'
	label var `evx' "e( `v' | X )"
	local xttl : var label `evx'
	if `"`plot'`addplot'"' == "" {
		local legend legend(nodraw)
	}
	sort `evx', stable
	version 8: graph twoway		///
	(scatter `resid' `evx'		///
		if `touse',		///
		ytitle(`"`yttl'"')	///
		xtitle(`"`xttl'"')	///
		note(`"`note'"')	///
		`legend'		///
		`options'		///
	)				///
	(lowess `resid' `evx') ///
	(line `hat' `evx',		///
		lstyle(refline)		///
		`rlopts'		///
	), legend(off)				///
	|| `plot' || `addplot'		///
	// blank
end


program define kavplots  /* avplots but calls kavplot rather than avplot */
	version 6.0
	if _caller() < 8 {
		avplots_7 `0'
		exit
	}
	local vv : display "version " string(_caller()) ":"

	_isfit cons newanovaok
	syntax [, * ]

	_get_gropts , graphopts(`options') getcombine getallowed(plot addplot)
	local options `"`s(graphopts)'"'
	local gcopts `"`s(combineopts)'"'
	if `"`s(plot)'"' != "" {
		di in red "option plot() not allowed"
		exit 198
	}
	if `"`s(addplot)'"' != "" {
		di in red "option addplot() not allowed"
		exit 198
	}		

	_getrhs rhs
	tokenize `rhs'

	while "`1'"!="" { 
		tempname tname
		local base `names'
		local names `names' `tname'
		capture `vv' kavplot `1', name(`tname') nodraw `options'
		if _rc { 
			if _rc!=399 {
				exit _rc
			} 
			local names `base'
		}
		mac shift 
	}

	version 8: graph combine `names' , `gcopts'
	version 8: graph drop `names'
end
