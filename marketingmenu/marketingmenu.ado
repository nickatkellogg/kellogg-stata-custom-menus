program define marketingmenu
 version 9
win m append separator stUser
win m append submenu stUser "Marketing Menu"

window menu append submenu "Marketing Menu" "Summarize and Describe Data"
window menu append item "Summarize and Describe Data" "Describe Data in Memory (describe)" "describe2"
window menu append item "Summarize and Describe Data" "View Data Labels (label list)" "db label_list"
win m append separator "Summarize and Describe Data"
window menu append item "Summarize and Describe Data" "Simple Summary Statistics (summarize)" "db summarize"
window menu append item "Summarize and Describe Data" "Complex Summary Statistics (tabstat)" "db tabstat"
win m append separator "Summarize and Describe Data"
window menu append item "Summarize and Describe Data" "Tabulate (tabulate)" "db tabulate1"
window menu append item "Summarize and Describe Data" "Cross-Tabulate (tabulate)" "db tabulate2"

window menu append submenu "Marketing Menu" "Manipulate Variables and Obs"
window menu append item "Manipulate Variables and Obs" "Generate New Variable (generate)" "db generate"
window menu append item "Manipulate Variables and Obs" "Extended Generate New Variable (egen)" "db egen"
window menu append item "Manipulate Variables and Obs" "Replace/Change Existing Variable (replace)" "db replace"
window menu append item "Manipulate Variables and Obs" "Create deciles or quintiles (xtile)" "db xtile"

win m append separator "Manipulate Variables and Obs"
window menu append item "Manipulate Variables and Obs" "Convert String Variable to Integer (encode)" "db encode"
window menu append item "Manipulate Variables and Obs" "View Data Labels (label list)" "db label_list"

win m append separator "Manipulate Variables and Obs"
window menu append item "Manipulate Variables and Obs" "Drop Variables (drop / keep)" "db drop_vars"
window menu append item "Manipulate Variables and Obs" "Drop Observations (drop if / keep if)" "db drop_obs"

window menu append submenu "Marketing Menu" "Graph Data"
window menu append item "Graph Data" "Draw Scatter Plot (scatter)" "db twoway"
window menu append item "Graph Data" "Draw Bar Graph (graph bar)" "db graph bar"
window menu append item "Graph Data" "Draw Histogram (histogram)" "db histogram"

win m append separator "Marketing Menu"
window menu append submenu "Marketing Menu" "Simple Tests of Association"
window menu append item "Simple Tests of Association" "Cross-Tabulate with Chi-Squared Test (tabulate)" "db tabulate2"
window menu append item "Simple Tests of Association" "Test of Means (ttest)" "db ttest"
window menu append item "Simple Tests of Association" "Test of Proportions (prtest)" "db prtest"
window menu append item "Simple Tests of Association" "Correlation Between Variables (pwcorr)" "db pwcorr"

window menu append submenu "Marketing Menu" "Regular Regression (OLS)"
window menu append item "Regular Regression (OLS)" "Run Regression (regress)" "db regress"
window menu append item "Regular Regression (OLS)" "Run Regression w/ multiple equations (reg3)" "db reg3"
window menu append item "Regular Regression (OLS)" "Generate Predicted Values (predict)" "db predict"
window menu append item "Regular Regression (OLS)" "Test Significance of Coefficients (test)" "db test"
window menu append item "Regular Regression (OLS)" "Regression Diagnostic Plots (kregdiag)" "kregdiag"

window menu append submenu "Marketing Menu" "Logistic Regression"
window menu append item "Logistic Regression" "Run Logistic Regression (logistic)" "db logistic"
window menu append item "Logistic Regression" "Generate Predicted Values and Residuals (predict)" "db predict"
window menu append item "Logistic Regression" "Test Significance of Coefficients (test)" "db test"

window menu append submenu "Marketing Menu" "Model Performance"
window menu append item "Model Performance" "Lift Charts/Tables (lift)" "db lift"
window menu append item "Model Performance" "Gains Charts/Tables (gains)" "db gains"
window menu append item "Model Performance" "Graph Prediction (graphpred)" "db graphpred"

win m append separator "Marketing Menu"
window menu append submenu "Marketing Menu" "Factor Analysis"
window menu append item "Factor Analysis" "Pre-Factor Diagnostics (kprefactor)" "db kprefactor"
window menu append item "Factor Analysis" "Factor Analysis (kfactor)" "db kfactor"

window menu append submenu "Marketing Menu" "Cluster Analysis"
window menu append item "Cluster Analysis" "Pre-Cluster Diagnostic: Dendrogram (kdendro)" "db kdendro"
window menu append item "Cluster Analysis" "Kmeans Cluster Analysis (kcluster)" "db kcluster"
window menu append item "Cluster Analysis" "Post-Cluster Histogram Diagnostic Plot (kclusterhist)" "db kclusterhist"
window menu append item "Cluster Analysis" "Post-Cluster Pairwise Diagnostic Plot (kclusterplot)" "db kclusterplot"

window menu append submenu "Marketing Menu" "Product Maps"
window menu append item "Product Maps" "Multidimensional Scaling (mds)" "db mdslong"
window menu append item "Product Maps" "Configure Graph (graph)" "db mdsconfig"
window menu append item "Product Maps" "Perceptual Map (percmap)" "db percmap"

window menu append submenu "Marketing Menu" "Conjoint Analysis"
window menu append item "Conjoint Analysis" "Conjoint Analysis (conjoint)" "db conjoint"
window menu append item "Conjoint Analysis" "Drop Zero Partworths (dropzeros)" "dropzeros"

win m append separator "Marketing Menu"
window menu append submenu "Marketing Menu" "Set Stata Preferences"
window menu append item "Set Stata Preferences" "Restore Default Window Layout (window manage)" "windowprefs2"
win m append separator "Set Stata Preferences"
window menu append item "Set Stata Preferences" "Clear Memory (clear)" "clear2"
window menu append item "Set Stata Preferences" "Set Memory in Procedures (set matsize)" "db matsize"

window menu refresh

end
