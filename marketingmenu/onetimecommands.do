* This do file allows for installations of third party ado files or any other one-time command

quietly net from  https://www.kellogg.northwestern.edu/stata/estout/
net install estout, replace

* for ITBM and other marketing courses
quietly memory
if `r(M_data)'< 52428800 {
	clear matrix
	set memory 50m, permanently
	}
set logtype text, permanently

