program percmap
version 11
syntax varlist (min=2), PRODName(varlist max=1 min=1) [Scale(numlist max=1 >0) Fscale(numlist max=1 >0) Mscale(numlist max=1 >0) PRODColor(namelist max=1) ATTRColor(namelist max=1) only(string)]

if "`only'"~="products" & "`only'"~="attributes" & "`only'"~="" {
	display as error `"Option "only()" may only contain "products" or "attributes""'
	error 198
}

set more off
qui factor `varlist', pcf factor(2) mineigen(0)
qui rotate, normalize

capture drop percmapfs*
quietly predict percmapfs*, regression
matrix define flmatrix=e(r_L)

qui gen tmpa=percmapfs1^2 + percmapfs2^2
qui sum tmpa
local a=`r(max)'
drop tmpa

preserve
capture svmat flmatrix
qui gen tmpb=flmatrix1^2 + flmatrix2^2
qui sum tmpb
local b=`r(max)'
drop tmpb

restore
local stretch=sqrt(`a'/`b')

if "`only'"=="attributes" {
	local stretch=1
}

if "`scale'"=="" {
	local scale=1
}

if "`fscale'"=="" {
	local fscale=1
}

if "`mscale'"=="" {
	local mscale=1
}

if "`prodcolor'"=="" {
	local prodcolor = "navy"
}

if "`attrcolor'"=="" {
	local attrcolor = "maroon"
}


local fscale=`fscale'*.5
local mscale=`mscale'*.5
local lscale=`mscale'*.7

matrix flmatrix=flmatrix*`stretch'*`scale'

local varnames: rowfullnames flmatrix
local numvars = rowsof(flmatrix)

forvalues variable=1/`numvars' {
	local myvar: word `variable' of `varnames'
	local factor1 = flmatrix[`variable',1] 
	local factor2 = flmatrix[`variable',2] 
    local arrowlist `"`arrowlist' 0 0 `factor2' `factor1' `"`myvar'"' "'
	}

local allowedcolors1="black gs0 gs1 gs2 gs3 gs4 gs5 gs6 gs7 gs8 gs9 gs10 gs11 gs12 gs13 gs14 gs15 gs16 white blue bluishgray brown cranberry cyan dimgray dkgreen dknavy dkorange eggshell" 
local allowedcolors2="emerald forest_green gold gray green khaki lavender lime ltblue ltbluishgray ltkhaki magenta maroon midblue midgreen mint navy olive olive_teal orange orange_red pink purple red sand" 
local allowedcolors3="sandb sienna stone teal yellow ebg ebblue edkblue eltblue eltgreen emidblue erose none background bg foreground fg"

local prodcolorok=0
local attrcolorok=0

forvalues i=1/3 {
foreach color of local allowedcolors`i' {
	if "`color'"=="`prodcolor'" {
		local prodcolorok =`prodcolorok'+1
		}
	if "`color'"=="`attrcolor'" {
		local attrcolorok =`attrcolorok'+1
		}
	}
}
	
if "`prodcolorok'"=="0" {
		display as error "Unknown color choice for prodcolor; Setting to default."
		local prodcolor = "navy"
	}
if "`attrcolorok'"=="0" {
		display as error "Unknown color choice for attrcolor; Setting to default."
		local attrcolor = "maroon"
	}

local products_plot "(scatter percmapfs2 percmapfs1, mcolor(`prodcolor') mlabel(`prodname') msize(*`mscale') mlabcolor(`prodcolor')  mlabsize(*`fscale') aspectratio(1))"
local attributes_plot "(pcarrowi `arrowlist', headlabel mcolor(`attrcolor') msize(*`mscale') mlwidth(*`mscale') lwidth(*`lscale') lcolor(`attrcolor') mlabcolor(`attrcolor')  mlabsize(*`fscale') mlabgap(*2))"

if "`only'"=="products" {
	local attributes_plot ""
}

if "`only'"=="attributes" {
	local products_plot ""
}

twoway 	`products_plot' `attributes_plot', ///
		title("Perceptual Map") xtitle("Factor Score 1") ytitle("Factor Score 2") legend(label(1 "Products") /// 
		label(2 "Attributes")) yscale(noline) xscale(noline) yline(0, lstyle(grid) lw(*.7) lcolor(gs0)) xline(0, lstyle(grid) lw(*.7) lcolor(gs0)) 

end
