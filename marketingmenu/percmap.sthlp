{smcl}
{* *! version 1.1.3  2jan2011}{...}
help for {cmd:percmap}{right:dialog:  {dialog percmap}}
{hline}

{title:Title}

{p2colset 5 17 19 2}{...}
{p2col:percmap {hline 2}}Perceptual Map{p_end}
{p2colreset}{...}


{title:Syntax}

{p 8 18 2}
{bf:percmap} {varlist}, prodname({varname}) [options]

{synoptset 25 tabbed}{...}
{synopthdr}
{synoptline}

{synopt:{opt prodname(varname)}}is required and specifies a variable that identifies observations (rows) of the dataset.{p_end}
{synopt:{opt scale(#)}}scales the lengths of the product attribute vectors relative to the products' factor scores. This quantity must be greater than zero and the default is scale(1).{p_end}
{synopt:{opt fscale(#)}}fscale scales the font size used for the product and atrribute names. This quantity must be greater than zero and the default is fscale(1).{p_end}
{synopt:{opt mscale(#)}}scales the size of the product point markers and the attribute line and arrowhead thicknesses.  This quantity must be greater than zero and the default is mscale(1){p_end}
{synopt:{cmd:prodcolor(}{it:{help colorstyle}}{cmd:)}}is the color used for plotting the product markers and names. The default is red and allowable values can be viewed by clicking on colorstyle.{p_end}
{synopt:{cmd:attrcolor(}{it:{help colorstyle}}{cmd:)}}is the color used for plotting the attributes vectors and names. The default is navy and allowable values can be viewed by clicking on colorstyle.{p_end}

{synoptline}

{title:Menu}

{phang}
{bf:Users > Marketing Menu > Product Maps > Perceptual Map}
{p_end}
{phang}
(if the Kellogg User Menu is installed)
{p_end}

{title:Description}

{pstd}
{opt percmap} plots a perceptual map for the products prodname in the rows of the dataset using the attributes varlist supplied in the columns of the dataset. 
Two varimax-rotated principal component factors are obtained and each product's factor 
scores are plotted. 
Attribute vectors obtained from the factor loadings (suitably rescaled for plotting) are overlaid on the plot.

{title:Examples}

{phang}{cmd:. sysuse auto}{p_end}
{phang}{cmd:. percmap price-gear, prodname(make)}{p_end}
{phang}{cmd:. percmap price-gear, prodname(make) scale(2)}{p_end}
{phang}{cmd:. percmap price-gear, prodname(make) fscale(1.5) mscale(.5)}{p_end}
{phang}{cmd:. percmap price-gear, prodname(make) prodcolor(purple) attrcolor(green)}{p_end}

{title:Authors}

    Blakeley McShane, Kellogg School of Management, Northwestern University
	b-mcshane@kellogg.northwestern.edu

    Florian Zettelmeyer, Kellogg School of Management, Northwestern University
	f-zettelmeyer@kellogg.northwestern.edu
