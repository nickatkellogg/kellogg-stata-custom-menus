program define scatterlist

syntax varlist (numeric min=2) [, *]

tokenize `varlist'
local numvars: word count `varlist'

foreach var of numlist 2/`numvars' {
	scatter `1' ``var'', name(s`var', replace) nodraw `options'
	local plotlist="`plotlist' s`var'" 
	}

graph combine `plotlist', scale(.8)

end
