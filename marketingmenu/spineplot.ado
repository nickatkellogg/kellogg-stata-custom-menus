*! NJC 1.0.4 1 Nov 2007 
* NJC 1.0.3 30 Oct 2007 
* NJC 1.0.2 25 Oct 2007 
* NJC 1.0.1 19 Oct 2007
* NJC 1.0.0 17 Oct 2007
program spineplot
	version 8.2 
	// -levels- or -levelsof- must be present 
	if _caller() >= 9 local of "of" 

	forval i = 1/20 {
		local baropts `baropts' bar`i'(str asis)
	}
	syntax varlist(min=2 max=2 numeric) [fweight aweight/] [if] [in] ///
	[, MISSing PERCent `baropts' text(str asis) barall(str asis) * ]
	
	local defaultcolor = "navy maroon forest_green dkorange teal cranberry lavender khaki sienna emidblue emerald brown erose gold bluishgray"

	quietly {
		if "`missing'" != "" marksample touse, novarlist zeroweight
		else marksample touse, zeroweight
		count if `touse'
		if r(N) == 0 error 2000

		if `"`text'"' != "" { 
			gettoken textvar text : text, parse(",") 
			capture confirm variable `textvar' 
			if _rc { 
				di as err "`textvar' not a variable"
				exit 198 
			} 
			gettoken comma text : text, parse(",") 
		} 

		tokenize "`varlist'"
		args y x
		preserve
		tempvar f xf xF xmid tag yF ymid xshow

		keep if `touse'
		if "`exp'" == "" local exp = 1 
		bysort `x' `y': gen double `f' = sum(`exp') 
		by `x' `y': keep if _n == _N
		fillin `x' `y'
		replace `f' = 0 if `f' == .
		drop _fillin
		compress `f'

		if "`percent'" != "" {
			local factor 100
			local la 0 25 50 75 100
			local what "percent"
		}
		else {
			local factor 1
			local la 0 .25 .5 .75 1
			local what "fraction"
		}

		bysort `x' (`y'): gen `xf' = sum(`f')
		by `x' : replace `xf' = `xf'[_N]
		by `x' : gen byte `tag' = _n == 1
		gen `xF' = sum(`xf' * `tag')
		by `x': gen `xmid' = `xF'[_N] - 0.5 * `xf'[_N]
		local total = `xF'[_N]

		by `x' : gen `yF' = sum(`f')
		by `x' : gen `ymid' = `yF' - 0.5 * `f'
		by `x' : replace `ymid' = `factor' * `ymid'/ `yF'[_N]
		by `x' : replace `yF' = `factor' * `yF'/ `yF'[_N]

		local first = _N + 1
		expand 2 if `x' == `x'[_N]
		replace `yF' = . in `first'/l
		sort `y' `x' `yF'
		by `y' : gen `xshow' = cond(_n == 1, 0, `xF'[_n - 1])

		tokenize "`la'"
		forval i = 1/5 {
			local xla1 = (`i' - 1) * `total'/4
			local xla `" `xla' `xla1' "``i''" "'
		}

		local xtitle : var label `x'
		if `"`xtitle'"' == "" local xtitle `x'
		local ytitle : var label `y'
		if `"`ytitle'"' == "" local ytitle `y'

		levels`of' `x', local(levels)
		foreach l of local levels {
			su `xmid' if `x' == `l', meanonly
			local l : label (`x') `l'
			local XLA `" `XLA' `r(min)' "`l'" "'
		}

		drop `f' `xf' `xF' `tag'

		levels`of' `y', local(levels)
		local ny : word count `levels'
		tokenize `levels'
		forval i = `ny'(-1)1 {
			local pipe = cond(`i' > 1, "||", "")
			local I = `ny' - `i' + 1

			local tmpindex = mod(`i',15)
			if `tmpindex'==0 {
				local tmpindex = 15
			}
			local tmpcolor `: word `tmpindex' of `defaultcolor''

			local call `call' ///
			bar `yF' `xshow' if `y' == ``i'' ///
			, bartype(spanning) blcolor(bg) blw(medium) color(`tmpcolor') ///
			`barall' `bar`I'' `pipe'

			local which : label (`y') ``i''
			local lgnd `lgnd' `I' `"`which'"'
		}

		if `"`textvar'"' != "" { 
			local texttoplot mla(`textvar') mlabpos(0) 
		}
	}

	twoway `call'                                    ///
	xaxis(1 2)                                       ///
	xsc(r(0, `total') axis(1))                       ///
	xsc(r(0, `total') axis(2))                       ///
	xla(`XLA', axis(2) noticks)                      ///
	xla(`xla', axis(1))                              ///
	yaxis(1 2)                                       ///
	ysc(r(0, `factor'))                              /// 
	yla(`la', ang(h) nogrid axis(2))                 ///
	yla(none, ang(h) nogrid axis(1))                 ///
	xtitle(`"`xtitle'"', axis(2))                    ///
	xtitle(`"`what' by `xtitle'"', axis(1))          /// 
        ytitle(`"`what' by `ytitle'"', axis(2))          /// 
        ytitle("", axis(1))                              /// 
	legend(order(`lgnd') col(1) pos(3))              ///
	`options'                                        ///
	|| scatter `ymid' `xmid' if !missing(`textvar'), ///
	ms(none)                                         /// 
	`texttoplot' `text'  
end

