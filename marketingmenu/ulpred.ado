program define ulpred
version 12
syntax varlist(min=2), Predvar(name) IDvar(varname)

qui sum `idvar'

qui gen `predvar'=.
foreach i of numlist `r(min)' / `r(max)' {
   capture qui reg `varlist' if user==`i'
   capture qui predict temp if user==`i'
   capture qui replace `predvar'=temp if user==`i'
   capture qui drop temp
   }
end
