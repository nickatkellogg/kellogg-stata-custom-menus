program define menuchoice
 version 10

* Is this redundant? Why mess with it if it works?
net set ado PERSONAL
capture net set other PERSONAL
capture net set other "`c(sysdir_stata)'"

* This executes the building of the first part of the menu, irrespective of what options students chose
startmenu

if `1' {
* Core Statistics Menu --> Owner: Peter Klibanoff
* Short name: coremenu
	menuinstall corestatisticsmenu
 }

 if `2' {
* Marketing Menu --> Owner: Florian Zettelmeyer
* Short name: marketingmenu
	menuinstall marketingmenu
 }

/*
 if `3' {
* Empirical Methods in Strategy Menu --> Owner: Leemore Dafny
* Short name: strategymenu
	menuinstall strategymenu
 }
*/

/* THIS IS WHERE ADDITIONAL COURSES WILL BE ADDED WITH THE SYNTAX
 if `#' {
* SOME COURSE NAME --> Owner: SOME FACULTY NAME
* Short name: genericcoursemenu
	menuinstall genericcoursemenu
 }
*/

* This executes the building of the last part of the menu
endmenu

* This resets stata install options to defaults (I think done anyway on restart)
capture net set ado PLUS
capture net set other

* The next 13 lines append "endmenu" to the sysprofile.

if c(os) == "Windows"{
	capture file open sysprofile1 using "c:\Data\sysprofile.do", write append
	capture file open sysprofile2 using "c:\ado\personal\sysprofile.do", write append
	capture file open sysprofile3 using "`c(sysdir_stata)'sysprofile.do", write append
	capture file write sysprofile1 "endmenu" _n
	capture file write sysprofile2 "endmenu" _n
	capture file write sysprofile3 "endmenu" _n
	capture file close sysprofile1
	capture file close sysprofile2
	capture file close sysprofile3
}

if  c(os) == "MacOSX" {
	capture file open sysprofile3 using "`c(sysdir_stata)'sysprofile.do", write append
	capture file write sysprofile3 "endmenu" _n
	capture file close sysprofile3
}

if  c(os) == "Unix" {
	capture file open sysprofile3 using "`c(sysdir_personal)'sysprofile.do", write append
	capture file write sysprofile3 "endmenu" _n
	capture file close sysprofile3
}


* This display the "its done" message
window stopbox note "The Kellogg Custom Menu has been installed. You can access it from the ''User'' menu."

end
