version 10 
set more off
clear

* The next 10 lines set up the directory structure for mac and windows

if  c(os) == "MacOSX" {
	capture mkdir  "~/Library/Application Support/Stata/"
	capture mkdir  "~/Library/Application Support/Stata/ado/"
	capture mkdir  "~/Library/Application Support/Stata/ado/personal/"
	capture mkdir  "~/Library/Application Support/Stata/ado/plus/"
	capture mkdir  "~/Library/Application Support/Stata/ado/stbplus/"
}

if c(os) == "Windows"{
	capture mkdir c:\ado
	capture mkdir c:\ado\personal
	capture mkdir c:\ado\plus
	capture mkdir c:\ado\stbplus
	capture mkdir c:\Data
}

if c(os) == "Unix" {
	capture mkdir "~/ado/"
	capture mkdir "~/ado/personal/"
	capture mkdir "~/ado/plus/"
	capture mkdir "~/ado/stbplus/"
}

* This tells stata to load files from the kellogg site
quietly net from  https://www.kellogg.northwestern.edu/stata/menuchoice/

* This tells stata to load all files into the personal ado subdirectory
net set ado PERSONAL
capture net set other PERSONAL
capture net set other "`c(sysdir_stata)'"

if c(os) == "Unix" {
	net set other PERSONAL
}

* Here we make a backup copy of the sysprofile.do file
if c(os) == "Windows"{
	capture copy "c:\Data\sysprofile.do" "c:\Data\sysprofile_beforemenu.do"
	capture copy "c:\ado\personal\sysprofile.do" "c:\ado\personal\sysprofile_beforemenu.do"
}

if  c(os) == "MacOSX" {
	capture copy "`c(sysdir_stata)'sysprofile.do" "`c(sysdir_stata)'/sysprofile_beforemenu.do"
}

if  c(os) == "Unix" {
	capture copy "`c(sysdir_personal)'sysprofile.do" "`c(sysdir_personal)'/sysprofile_beforemenu.do"
}

* This installs all relevant files for the menu installation into the local stata directory
net install menuchoice, all replace

* This calls up the menu choice dialog box
db menuchoice

