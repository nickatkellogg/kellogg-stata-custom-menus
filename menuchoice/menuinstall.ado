program define menuinstall
version 10

* The next 13 lines append the chosen menu to the sysprofile.

if c(os) == "Windows"{
	capture file open sysprofile1 using "c:\Data\sysprofile.do", write append
	capture file open sysprofile2 using "c:\ado\personal\sysprofile.do", write append
	capture file open sysprofile3 using "`c(sysdir_stata)'sysprofile.do", write append
	capture file write sysprofile1 "`1'" _n
	capture file write sysprofile2 "`1'" _n
	capture file write sysprofile3 "`1'" _n
	capture file close sysprofile1
	capture file close sysprofile2
	capture file close sysprofile3
}

if  c(os) == "MacOSX" {
	capture file open sysprofile3 using "`c(sysdir_stata)'sysprofile.do", write append
	capture file write sysprofile3 "`1'" _n
	capture file close sysprofile3
}

if  c(os) == "Unix" {
	capture file open sysprofile3 using "`c(sysdir_personal)'sysprofile.do", write append
	capture file write sysprofile3 "`1'" _n
	capture file close sysprofile3
}

* This executes a do file that does installations of third party ado files or any other one-time commands
* on a course by course basis. See the "marketingmenu" directory for an example.
capture do https://www.kellogg.northwestern.edu/stata/`1'/onetimecommands.do

* This tells stata to install from the kellog website
quietly net from  https://www.kellogg.northwestern.edu/stata/`1'/
* This uses the stata net install facility to install all files needed for the menu
net install `1', all replace 
`1'

end 
