version 10 
set more off
clear

* This tells stata to load files from the kellogg site
quietly net from  https://www.kellogg.northwestern.edu/stata/marketingmenu/
* This tells stata to load all files into the personal ado subdirectory
net set ado PERSONAL
capture net set other PERSONAL
capture net set other "`c(sysdir_stata)'"

* This installs all relevant files for the menu installation into the local stata directory
ado uninstall marketingmenu
