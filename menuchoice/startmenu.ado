program define startmenu

version 9
window menu clear
      win m append separator stUser
window menu append item stUser "--- KELLOGG CUSTOM MENU ---" ""

      win m append separator stUser
window menu append submenu "stUser" "Record your work"
window menu append item "Record your work" "Open Log (log using)" "db log2"
window menu append item "Record your work" "Close Log (log close)" "logclose2"

win m append separator stUser
window menu append submenu "stUser" "Load Data ..."
window menu append item "Load Data ..." "Stata Dataset (use)" "use_dlg"
window menu append item "Load Data ..." "Excel Spreadsheet (import excel)" "db import excel"
window menu append item "Load Data ..." "ASCII (text) data created by a spreadsheet (insheet)" "db insheet"

window menu append submenu "stUser" "Save Data ..."
window menu append item "Save Data ..." "Stata Dataset (save)" "save_dlg"
window menu append item "Save Data ..." "Excel Spreadsheet (export excel)" "db export excel"
window menu append item "Save Data ..." "ASCII (text) data readable by a spreadsheet (outsheet)" "db outsheet"

      win m append separator "stUser"
window menu append item "stUser" "Browse Data (&browse)" "db browse"
window menu append item "stUser" "Edit/Modify Data (&edit)" "db edit"

end
