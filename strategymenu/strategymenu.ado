**** THIS CONTAINS THE FULL MENU *** 
**** PLEASE BUILD YOUR MENU -- HERE IS AN EXAMPLE MENU *****

program define strategymenu 
 version 9
      win m append separator stUser
      win m append submenu stUser "Empirical Methods in Strategy Menu"

	/*
window menu append submenu "Marketing Menu" "   Record your work"
window menu append item "   Record your work" "Open Results Log (log using)" "db log2"
window menu append item "   Record your work" "Close Results Log (log close)" "logclose2"
      win m append separator "   Record your work"
window menu append item "   Record your work" "Open Commands Log (cmdlog using)" "db cmdlog2"
window menu append item "   Record your work" "Close Commands Log (cmdlog close)" "cmdlogclose2"

window menu append submenu "Marketing Menu" "   Open/Save Data"
window menu append item "   Open/Save Data" "Open Stata Dataset (use) -> File Menu (''Open'') or Toolbar" "db use"
window menu append item "   Open/Save Data" "Save Stata Dataset (save) -> File Menu (''Save'') or Toolbar" "db save"
      win m append separator "   Open/Save Data"
window menu append item "   Open/Save Data" "Import From Spreadsheet (insheet) -> File Menu (''Import'')" "db insheet"
window menu append item "   Open/Save Data" "Export To Spreadsheet (outsheet) -> File Menu (''Export'')" "db outsheet"

      win m append separator "Marketing Menu"
window menu append submenu "Marketing Menu" "   View Data as Spreadsheet"
window menu append item "   View Data as Spreadsheet" "Browse Data (browse) -> Toolbar" "browse2"
window menu append item "   View Data as Spreadsheet" "Edit/Modify Data (edit) -> Toolbar" "edit2"

window menu append submenu "Marketing Menu" "   Summarize and Describe Data"
window menu append item "   Summarize and Describe Data" "Describe Data in Memory (describe)" "describe2"
window menu append item "   Summarize and Describe Data" "View Data Labels (label list)" "db label_list"
      win m append separator "   Summarize and Describe Data"
window menu append item "   Summarize and Describe Data" "Simple Summary Statistics (summarize)" "db summarize"
window menu append item "   Summarize and Describe Data" "Complex Summary Statistics (tabstat)" "db tabstat"
      win m append separator "   Summarize and Describe Data"
window menu append item "   Summarize and Describe Data" "Tabulate (tabulate)" "db tabulate1"
window menu append item "   Summarize and Describe Data" "Cross-Tabulate (tabulate)" "db tabulate2"

window menu append submenu "Marketing Menu" "   Graph Data"
window menu append item "   Graph Data" "Draw Scatter Plot (scatter)" "db twoway"
window menu append item "   Graph Data" "Draw Bar Graph (graph bar)" "db graph bar"
window menu append item "   Graph Data" "Draw Histogram (histogram)" "db histogram"

      win m append separator "Marketing Menu"
window menu append submenu "Marketing Menu" "   Create and Modify Variables"
window menu append item "   Create and Modify Variables" "Generate New Variable (generate)" "db generate"
window menu append item "   Create and Modify Variables" "Generate New Variable on Steroids (egen)" "db egen"
window menu append item "   Create and Modify Variables" "Replace/Change Existing Variable (replace)" "db replace"
window menu append item "   Create and Modify Variables" "Create deciles or quintiles (xtile)" "db xtile"

window menu append submenu "Marketing Menu" "   Manage String Variables"
window menu append item "   Manage String Variables" "Convert String Variable to Integer (encode)" "db encode"
window menu append item "   Manage String Variables" "View Data Labels (label list)" "db label_list"

window menu append submenu "Marketing Menu" "   Drop/Keep Variables and Observations"
window menu append item "   Drop/Keep Variables and Observations" "Drop Variables (drop / keep)" "db drop_vars"
window menu append item "   Drop/Keep Variables and Observations" "Drop Observations (drop if / keep if)" "db drop_obs"

      win m append separator "Marketing Menu"
window menu append submenu "Marketing Menu" "   Simple Tests of Association"
window menu append item "   Simple Tests of Association" "Cross-Tabulate with chi-squared test (tabulate)" "db tabulate2"
window menu append item "   Simple Tests of Association" "Test of Means (ttest)" "db ttestby"
window menu append item "   Simple Tests of Association" "Test of Proportions (prtest)" "db prtestby"
window menu append item "   Simple Tests of Association" "Correlation Between Variables (pwcorr)" "db pwcorr"

window menu append submenu "Marketing Menu" "   Regular Regression (OLS)"
window menu append item "   Regular Regression (OLS)" "Run Regression (regress)" "db regress"
window menu append item "   Regular Regression (OLS)" "Run Regression w/ multiple equations (reg3)" "db reg3"
window menu append item "   Regular Regression (OLS)" "Generate Predicted Values (predict)" "db predict"
window menu append item "   Regular Regression (OLS)" "Test Significance of Coefficients (test)" "db test"

window menu append submenu "Marketing Menu" "   Logistic Regression"
window menu append item "   Logistic Regression" "Run Logistic Regression (logistic)" "db logistic"
window menu append item "   Logistic Regression" "Generate Predicted Values (predict)" "db predict"
window menu append item "   Logistic Regression" "Test Significance of Coefficients (test)" "db test"

      win m append separator "Marketing Menu"
window menu append submenu "Marketing Menu" "   Set Stata Preferences"
window menu append item "   Set Stata Preferences" "Restore Default Window Layout (window manage)" "windowprefs2"
      win m append separator "   Set Stata Preferences"
window menu append item "   Set Stata Preferences" "Clear Memory (clear)" "clear2"
window menu append item "   Set Stata Preferences" "Set Memory for Data (set memory)" "db memory"
window menu append item "   Set Stata Preferences" "Set Memory in Procedures (set matsize)" "db matsize"

*/

window menu refresh

end
